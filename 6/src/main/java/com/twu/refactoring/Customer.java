package com.twu.refactoring;

import java.util.ArrayList;
import java.util.Iterator;

public class Customer {

	private String name;
	private ArrayList<Rental> rentals = new ArrayList<Rental>();

	public Customer(String name) {
		this.name = name;
	}

	public void addRental(Rental arg) {
		rentals.add(arg);
	}

	public String getName() {
		return name;
	}

	public String statement() {
		double totalAmount = 0;
		int frequentRenterPoints = 0;
		Iterator<Rental> rentals = this.rentals.iterator();
		String lines = getHeaderLine();
		while (rentals.hasNext()) {
			double thisAmount = 0;
			Rental each = rentals.next();
			thisAmount = each.getThisAmount(thisAmount);
			frequentRenterPoints = setFrequentRenterPoints(frequentRenterPoints, each);

			lines = showFiguresForRental(lines, thisAmount, each);
			totalAmount += thisAmount;

		}
		lines = getFooterLines(totalAmount, frequentRenterPoints, lines);
		return lines;
	}

	private String showFiguresForRental(String result, double thisAmount, Rental each) {
		result += "\t" + each.getMovie().getTitle() + "\t"
				+ String.valueOf(thisAmount) + "\n";
		return result;
	}

	private int setFrequentRenterPoints(int frequentRenterPoints, Rental each) {
		frequentRenterPoints++;
		if ((each.getMovie().getPriceCode() == Movie.NEW_RELEASE)
				&& each.getDaysRented() > 1)
			frequentRenterPoints++;
		return frequentRenterPoints;
	}

	private String getFooterLines(double totalAmount, int frequentRenterPoints, String result) {
		result += "Amount owed is " + String.valueOf(totalAmount) + "\n";
		result += "You earned " + String.valueOf(frequentRenterPoints)
				+ " frequent renter points";
		return result;
	}

	private String getHeaderLine() {
		return "Rental Record for " + getName() + "\n";
	}

}
