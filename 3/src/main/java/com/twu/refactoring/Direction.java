package com.twu.refactoring;

public class Direction {
    private final char direction;

    public Direction(char direction) {
        this.direction = direction;
    }

    public Direction turnRight() {
        return getDirection('E', 'W');
    }

    public Direction turnLeft() {
        return getDirection('W', 'E');
    }

    private Direction getDirection(char w, char e) {
        if (direction == 'N') {
            return new Direction(w);
        } else if (direction == 'S') {
            return new Direction(e);
        } else if (direction == 'E') {
            return new Direction('N');
        } else if (direction == 'W') {
            return new Direction('S');
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public boolean equals(Object direction) {
        if (this == direction) return true;
        if (direction == null || getClass() != direction.getClass()) return false;

        Direction truhyDirection = (Direction) direction;

        if (this.direction != truhyDirection.direction) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) direction;
    }

    @Override
    public String toString() {
        return "Direction{direction=" + direction + '}';
    }
}
