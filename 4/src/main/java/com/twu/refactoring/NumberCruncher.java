package com.twu.refactoring;

public class NumberCruncher {
    private final int[] numbers;

    public NumberCruncher(int... numbers) {
        this.numbers = numbers;
    }

    public int countEven() {
        int count = initCount();
        for (int number : numbers) {
            if (isEven(number, 0)) count++;
        }
        return count;
    }

    private int initCount() {
        return 0;
    }

    private boolean isEven(int number, int i) {
        return number % 2 == i;
    }

    public int countOdd() {
        int count = initCount();
        for (int number : numbers) {
            if (isEven(number, 1)) count++;
        }
        return count;
    }

    public int countPositive() {
        int count = initCount();
        for (int number : numbers) {
            if (isGreaterZero(number)) count++;
        }
        return count;
    }

    private boolean isGreaterZero(int number) {
        return number >= 0;
    }

    public int countNegative() {
        int count = initCount();
        for (int number : numbers) {
            if (isLittleZero(number)) count++;
        }
        return count;
    }

    private boolean isLittleZero(int number) {
        return number < 0;
    }
}
