package com.twu.refactoring;

/**
 * OrderReceipt prints the details of order including customer name, address, description, quantity,
 * price and amount. It also calculates the sales tax @ 10% and prints as part
 * of order. It computes the total order amount (amount of individual lineItems +
 * total sales tax) and prints it.
 *
 */
public class OrderReceipt {
    private Order order;

    public OrderReceipt(Order order) {
        this.order = order;
	}

	public String printReceipt() {
		StringBuilder receipt = new StringBuilder();
		printBasicInfo(receipt);

		double totSalesTx = 0d;
		double tot = 0d;
		for (LineItem lineItem : order.getLineItems()) {
			lineItem.printDetailInfo(receipt, lineItem);
			totSalesTx += calculateTotalAmountOfLineItem(lineItem.totalAmount());
            tot += lineItem.totalAmount() + calculateTotalAmountOfLineItem(lineItem.totalAmount());
		}

		printFooterInfo(receipt, totSalesTx, tot);

		return receipt.toString();
	}

	private void printFooterInfo(StringBuilder receipt, double totSalesTx, double tot) {
		receipt.append("Sales Tax").append('\t').append(totSalesTx);
		receipt.append("Total Amount").append('\t').append(tot);
	}



	private void printBasicInfo(StringBuilder receipt) {
		receipt.append("======Printing Orders======\n");
		receipt.append(order.getCustomerName());
		receipt.append(order.getCustomerAddress());
	}

	private  double calculateTotalAmountOfLineItem(double amount) {
		return amount * .10;
	}






}
